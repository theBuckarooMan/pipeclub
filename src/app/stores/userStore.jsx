var AppDispatcher = require('../dispatcher/dispatcher.jsx');
var UserConstants = require('../constants/userStoreConstants.jsx');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');


function getDefaultUser(){
	return {
				username : "Stranger",
			    password  : "",
			    isLoggedIn : false,
			    open : false
			};
}

var _user = getDefaultUser();

function loadDefaultUser(){
	_user = getDefaultUser();
}

function loadUser(data){
	_user = data; 
}


var UserStore = assign({}, EventEmitter.prototype, {

	getUser: function(){
		return _user;
	},

	emitChange: function() {
	    this.emit('change');
	  },

	  addChangeListener: function(callback) {
	    this.on('change', callback);
	  },

	  removeChangeListener: function(callback) {
	    this.removeListener('change', callback);
	  }
});

AppDispatcher.register(function(payload){
	var action = payload.action;
	var text;

	 // Define what to do for certain actions
  	switch(action.actionType) {
	    case UserConstants.LOAD_USER:
	      // Call internal method based upon dispatched action
	      loadUser(action.data);
	      break;
	    case UserConstants.LOAD_DEFAULT_USER:
	      loadDefaultUser();
	      break;

	    default:
	      return true;
	}

	UserStore.emitChange();

	return true;
});

module.exports = UserStore;
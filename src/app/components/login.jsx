const React = require('react');
const RaisedButton = require('material-ui/lib/raised-button');
const FlatButton = require('material-ui/lib/flat-button');
const TextField = require('material-ui/lib/text-field');
const UserActions = require('../actions/userStoreActions.jsx');

function checkCredentials(){
	if(this.state.username=="Mario" && this.state.password=="1234"){
		
	}
};

const Login = React.createClass({
	getInitialState : function() {
		var user = {
			      username : "",
			      password  : "",
			      isLoggedIn : false,
			      open : false
			    };
	    return user;
  	},
  	handleUsername: function(event) {
    	this.setState({username: event.target.value});
    	this.setState({isLoggedIn: true});
  	},
  	handlePassword: function(event) {
    	this.setState({password: event.target.value});
  	},
  	handleOpen: function(event) {
    	this.setState({open: true});
  	},
  	handleSkip: function(event) {
  		this.setState({isLoggedIn: false});
  		UserActions.loadDefaultUser();
    	this.setState({open: false});
  	},
  	handleClose: function(event) {
  		UserActions.loadUser(this.state);
    	this.setState({open: false});
  	},
	render: function() {

	    var loginElement;
		if(this.state.open){
		    loginElement = 
		      <div style={loginContainerStyle}>
		      	<div style={loginInputContainerStyle}>
		      		<div style={GetInviteButtonStyle}>GET INVITE</div>
			      	<div style={loginHeaderStyle}>LOGIN</div>
			        <TextField
			        	value={this.state.username}
			        	onChange={this.handleUsername}
						hintText="MAIL"
						hintStyle={hintStyle}
						style={textFieldStyle}
						inputStyle={inputStyle}
						underlineStyle={underlineStyle}
						underlineFocusStyle={underlineFocusStyle}/>

			       	<TextField
			       		value={this.state.password}
			        	onChange={this.handlePassword}
						hintText="PASSWORD"
						hintStyle={hintStyle}
						style={textFieldStyle}
						inputStyle={inputStyle}
						underlineStyle={underlineStyle}
						underlineFocusStyle={underlineFocusStyle}
						type="password" />
					<div style={passwordForgotStyle}>FORGOT</div>
				</div>
				<div style={loginControlsContainerStyle}>
					 <div style={LoginControlsButtonStyle} onTouchTap={this.handleClose}>OK</div>
					 <div style={LoginControlsButtonStyle} onTouchTap={this.handleSkip}>SKIP</div>
				</div>
		      </div>
		}
		else{
		    loginElement = 
		      <div style={LoginButtonStyle} onTouchTap={this.handleOpen}>LOGIN</div>
		}

	    return (
		    <div>
		        {loginElement}
		    </div>
		);
	}

});

module.exports = Login;

//styles
var loginContainerStyle={
  	minHeight: "100%",
  	position: "fixed",
  	top : "0",
  	left : "0",
  	width : "100%",
  	height : "100%",
  	overlfow : "hidden"
};

var loginInputContainerStyle={
	display: "flex",
  	justifyContent: "center",
  	alignItems: "flex-start",
  	flexDirection: "column",
  	background: "#1E1B17",
  	height : "70%",
  	padding:"2rem"
};

var loginControlsContainerStyle={
	display: "flex",
  	justifyContent: "space-between",
  	alignItems: "flex-start",
  	flexDirection: "row",
  	background: "#212121",
  	height : "30%",
  	padding:"2rem"
};

var loginHeaderStyle={
	fontSize:"3.5rem",
	marginBottom:"5rem",
	marginTop:"4rem"
};

var GetInviteButtonStyle={ 
	padding:"0.5rem 1rem",
  	fontWeight:"400",
  	fontSize:"1.2rem",
  	border:"1px solid",
  	borderRadius:"50",
  	background:"white",
  	color:"black",
  	marginTop:"4rem"
};

var LoginButtonStyle={ 
	padding:"0.5rem 1rem",
  	fontWeight:"300",
  	fontSize:"1.2rem",
  	border:"1px solid",
  	borderRadius:"50"
};

var LoginControlsButtonStyle={ 
	padding:"0.5rem 1rem",
  	fontWeight:"500",
  	fontSize:"1.2rem",
  	border:"1px solid",
  	borderRadius:"50"
};

var textFieldStyle={
	fontSize:"3rem", 
	width:"75%",
	marginBottom:"2rem",
};

var inputStyle={
	color:"white"
};

 var underlineStyle={
 	bottom:"-10",
 };

 var underlineFocusStyle={
 	borderColor:"#D7BC84"
 };

 var hintStyle={
 	color: 'gray'
 };

 var passwordForgotStyle={
 	marginTop:"1rem",
 	fontWeight:"bolder",
 	fontSize:"small"
 };
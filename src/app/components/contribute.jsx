var React = require('react');

var Contribute = React.createClass({
	render: function(){
		return(
			<div style={contributeStyle}>CONTRIBUTE</div>
		);
	}
});

module.exports=Contribute;

//styles
var contributeStyle={ 
	padding:"0.5rem 1rem",
  	fontWeight:"300",
  	fontSize:"1.2rem",
  	border:"1px solid",
  	borderRadius:"50"
};
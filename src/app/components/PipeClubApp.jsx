const React = require('react');
const RaisedButton = require('material-ui/lib/raised-button');
const AppBar = require('material-ui/lib/app-bar');
const IconMenu = require('material-ui/lib/menus/icon-menu');
const MenuItem = require('material-ui/lib/menus/menu-item');
import { NavigationMenu as MenuIcon } from 'material-ui/lib/svg-icons/';
const IconButton = require('material-ui/lib/icon-button');
const Loader = require('./loader.jsx');
const Searchbar=require('./searchbar.jsx');
const Navbar=require('./navbar.jsx');
const Contribute=require('./contribute.jsx')
const Login=require('./login.jsx')
const UserStore = require('../stores/userStore.jsx');

// Method to retrieve user state from store
function getUserState() {
  return {
    user: UserStore.getUser()
  };
}

const PipeClubApp = React.createClass({
  // Use getUserState method to set initial state
  getInitialState: function() {
    return getUserState();
  },
  // Listen for changes
  componentDidMount: function() {
    UserStore.addChangeListener(this._onChange);
  },

  // Unbind change listener
  componentWillUnmount: function() {
    UserStore.removeChangeListener(this._onChange);
  },

	render: function() {
    var contributeElement;
    if(this.state.user.isLoggedIn){
      contributeElement=<Contribute/>
    }
    else{
      contributeElement=<Login/>
    }


    return (
      <div>
        <Loader />
        
       <AppBar iconElementLeft={<div style={logoStyle}></div>} style={appBarStyle} 
       iconElementRight={
        <IconMenu iconButtonElement={
          <IconButton><MenuIcon color="white" hoverColor="#D7BC84"/></IconButton>
        }>
          <MenuItem primaryText="Search" />
          <MenuItem primaryText="Help" />
          <MenuItem primaryText="Login" />
        </IconMenu>
       }/>


        <div style={appContainerStyle}>
        	<div style={headLine1Style}>Live it.<br/>Love it.<br/>Ride it.</div>
        	<br/>
        	<br/>
        	<div>
        		<div style={headline2Style}>Hello {this.state.user.username}</div>
        		<div style={headline3Style}>
        			Discover projects, routes or events.
        			<br/>
					Get inspired and take action.	
					<div style={actionButtonsStyle}>
		        		<Searchbar/>
		        		{contributeElement}
		        	</div>
				</div>
        	</div>
        	
        </div>
      </div>
    );
  },

  // Update view state when change event is received
  _onChange: function() {
    this.setState(getUserState());
  }
});

module.exports = PipeClubApp;

//styles
var appContainerStyle={
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  flexDirection: "column",
  minHeight: "100vh",
  backgroundImage: 'url(assets/img/background.png)',
  backgroundPosition: "center center",
  backgroundRepeat: "noRepeat",
  overflow: "hidden",
  color: "white"
};

var headLine1Style={
	fontSize: "4rem", 
  	fontWeight:"300",
  	textAlign:"left",
  	margin:"1rem"
};

var headline2Style={
	fontSize: "3rem", 
  	fontWeight:"200",
  	textAlign:"center",
  	margin:"1rem"
};

var headline3Style={
	fontSize: "1.2rem", 
  	fontWeight:"300",
  	textAlign:"center",
  	margin: "1rem"
};

var actionButtonsStyle={
	display: "flex",
	justifyContent: "center",
	alignItems: "center",
	flexDirection: "row"
	
};

var logoStyle={
  backgroundImage: 'url(assets/img/logoSmallWhite.png)',
    width: "60px",
    height:"60px"
};

var menuButtonStyle={
  color:"black"
};

var appBarStyle={
  background: "#212121"
};


var React=require('react');

var Navbar = React.createClass({
	render: function(){
		return(
			<div style={navbarStyle}>
				<div style={logoStyle}></div>
				<div style={menuButtonStyle}></div>
			</div>
		);
	}
});

module.exports=Navbar;

//styles
var navbarStyle={ 
	padding:"0.5rem",
	display: "flex",
	justifyContent: "space-between",
	alignItems: "center",
	background: "white",
	top:"0"
};

var logoStyle={
	backgroundImage: 'url(assets/img/logoSmall.png)',
  	width: "60px",
  	height:"60px"
};

var menuButtonStyle={
	backgroundImage: 'url(assets/img/menu.svg)',
  	width: "30px",
  	height:"30px",
  	backgroundSize: "30px 30px"
};
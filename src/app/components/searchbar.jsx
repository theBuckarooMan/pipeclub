var React = require('react');

var Searchbar = React.createClass({
	render: function(){
		return(
			<div style={searchbarStyle}>DISCOVER</div>
		);
	}
});

module.exports=Searchbar;

//styles
var searchbarStyle={ 
	padding:"0.5rem 1rem",
  	fontWeight:"300",
  	fontSize:"1.2rem",
  	border:"1px solid",
  	borderRadius:"50",
  	margin:"1rem"
};
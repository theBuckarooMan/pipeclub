var AppDispatcher = require('../dispatcher/dispatcher.jsx');
var UserStoreConstants = require('../constants/userStoreConstants.jsx');

var UserStoreActions = {

  loadUser: function(data) {
    AppDispatcher.handleViewAction({
      actionType: UserStoreConstants.LOAD_USER,
      data: data
    })
  },
  loadDefaultUser: function() {
    AppDispatcher.handleViewAction({
      actionType: UserStoreConstants.LOAD_DEFAULT_USER,
      data: null
    })
  }

};

module.exports = UserStoreActions;
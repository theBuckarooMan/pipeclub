var keyMirror = require('keyMirror');

module.exports = keyMirror({
  LOAD_USER: null,
  LOAD_DEFAULT_USER: null
});